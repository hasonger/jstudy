package sprout.jstudy.ssi.dao.user.impl;

import sprout.jstudy.ssi.dao.BaseDao;
import sprout.jstudy.ssi.dao.user.UserDao;
import sprout.jstudy.ssi.domain.user.User;
import sprout.jstudy.ssi.domain.user.UserQuery;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-16
 * Time: 下午2:56
 * To change this template use File | Settings | File Templates.
 */
public class UserDaoImpl extends BaseDao implements UserDao {

    @Override
    public boolean add(User user) {
        Long id = (Long)this.insert("user.add", user);
        return id > 0;
    }

    @Override
    public boolean update(User user) {
        return this.update("user.update", user) == 1;
    }

    @Override
    public User getById(long id) {
        return (User) this.queryForObject("user.getById", id);
    }

    @Override
    public int count(UserQuery userQuery) {
        return (Integer) this.queryForObject("user.count", userQuery);
    }

    @Override
    public List<User> list(UserQuery userQuery) {
        return this.queryForList("user.list", userQuery);
    }
}
