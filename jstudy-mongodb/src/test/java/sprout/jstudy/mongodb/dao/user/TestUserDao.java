package sprout.jstudy.mongodb.dao.user;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import sprout.jstudy.mongodb.domain.user.User;

/**
 * Created by IntelliJ IDEA.
 * User: sprout
 * Date: 12-12-3
 * Time: 上午11:35
 * To change this template use File | Settings | File Templates.
 */
public class TestUserDao {

    @Test
    public void testAdd(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        UserDao userDao = (UserDao) context.getBean("userDaoMongoDB");
        for(int i = 0; i < 10000; i++){
            User user = new User();
            user.setName("张三");
            user.setSex(User.Sex.WOMAN.getKey());
            user.setAge(25);
            user.setMobile("18210599999");
            user.setAddress("北京");
            userDao.add(user);
        }
    }

}
